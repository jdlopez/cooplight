from datetime import datetime, timedelta
import pyowm
import RPi.GPIO as GPIO
from pytz import timezone
import time

owm = pyowm.OWM('1c661e2e0a2e8ce311865a1134d8ef4c')

observation = owm.weather_at_place('Chicago, US')

w = observation.get_weather()

'''
Timezone in Chicago from GMT
'''
chicagoSunsetTime = w.get_sunset_time(timeformat='unix')


print("\nChicago Sunset time")
print(datetime.fromtimestamp(chicagoSunsetTime))
print(chicagoSunsetTime)


while True:

    '''
    Current time rounded to the nearest int
    '''
    t = int(round(time.time()))
    
    isSunDown = chicagoSunsetTime < t
    print(isSunDown)
    print(chicagoSunsetTime)
    print(t) 
    time.sleep(1)
    
    if isSunDown:
        print ("\nSun is not out\n\n")
        #TODO turn light on for a short while
        
        GPIO.setmode(GPIO.BOARD)
        mypin = 7
        GPIO.setup(mypin, GPIO.OUT, initial = 1)
        print ("ON")
        time.sleep(10)
        print ("OFF")
        GPIO.output(mypin, 0)
        time.sleep(1)
        GPIO.cleanup()
        
        observation = owm.weather_at_place('Chicago, US')

        w = observation.get_weather()
        '''
        Timezone in Chicago from GMT
        '''
        chicagoSunsetTime = w.get_sunset_time(timeformat='unix')

        
        #print ("Turning on light!")
